Definitions
==================================

This is a sample app that uses Urban Dictionary API.

THe features it covers are:
- Search for a term.
- Display the results.
- Cache the data.
- Sort the results by thumbs up or down.

Architecture
------------

This app uses MVVM architecture + Repository Pattern + Databinding.


The solution was meant to be done in about 5 hours so I did my best to find a suitable solution for
the size of the app, the requested features and the quality of the code.






