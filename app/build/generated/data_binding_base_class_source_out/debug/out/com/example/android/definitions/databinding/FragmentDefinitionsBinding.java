// Generated by data binding compiler. Do not edit!
package com.example.android.definitions.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.Bindable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;
import com.example.android.definitions.R;
import com.example.android.definitions.viewmodels.DefinitionsViewModel;
import java.lang.Deprecated;
import java.lang.Object;

public abstract class FragmentDefinitionsBinding extends ViewDataBinding {
  @NonNull
  public final ProgressBar loadingSpinner;

  @NonNull
  public final RadioGroup orderByRadioGroup;

  @NonNull
  public final RadioButton orderByThumbsDown;

  @NonNull
  public final RadioButton orderByThumbsUp;

  @NonNull
  public final RecyclerView recyclerView;

  @NonNull
  public final TextView search;

  @NonNull
  public final EditText wordEditext;

  @Bindable
  protected DefinitionsViewModel mViewModel;

  protected FragmentDefinitionsBinding(Object _bindingComponent, View _root, int _localFieldCount,
      ProgressBar loadingSpinner, RadioGroup orderByRadioGroup, RadioButton orderByThumbsDown,
      RadioButton orderByThumbsUp, RecyclerView recyclerView, TextView search,
      EditText wordEditext) {
    super(_bindingComponent, _root, _localFieldCount);
    this.loadingSpinner = loadingSpinner;
    this.orderByRadioGroup = orderByRadioGroup;
    this.orderByThumbsDown = orderByThumbsDown;
    this.orderByThumbsUp = orderByThumbsUp;
    this.recyclerView = recyclerView;
    this.search = search;
    this.wordEditext = wordEditext;
  }

  public abstract void setViewModel(@Nullable DefinitionsViewModel viewModel);

  @Nullable
  public DefinitionsViewModel getViewModel() {
    return mViewModel;
  }

  @NonNull
  public static FragmentDefinitionsBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.fragment_definitions, root, attachToRoot, component)
   */
  @NonNull
  @Deprecated
  public static FragmentDefinitionsBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable Object component) {
    return ViewDataBinding.<FragmentDefinitionsBinding>inflateInternal(inflater, R.layout.fragment_definitions, root, attachToRoot, component);
  }

  @NonNull
  public static FragmentDefinitionsBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.fragment_definitions, null, false, component)
   */
  @NonNull
  @Deprecated
  public static FragmentDefinitionsBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable Object component) {
    return ViewDataBinding.<FragmentDefinitionsBinding>inflateInternal(inflater, R.layout.fragment_definitions, null, false, component);
  }

  public static FragmentDefinitionsBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.bind(view, component)
   */
  @Deprecated
  public static FragmentDefinitionsBinding bind(@NonNull View view, @Nullable Object component) {
    return (FragmentDefinitionsBinding)bind(component, view, R.layout.fragment_definitions);
  }
}
