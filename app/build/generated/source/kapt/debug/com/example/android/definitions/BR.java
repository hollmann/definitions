package com.example.android.definitions;

public class BR {
  public static final int _all = 0;

  public static final int definition = 1;

  public static final int definitionCallback = 2;

  public static final int viewModel = 3;
}
