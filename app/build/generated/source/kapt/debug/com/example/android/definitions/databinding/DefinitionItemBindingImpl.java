package com.example.android.definitions.databinding;
import com.example.android.definitions.R;
import com.example.android.definitions.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class DefinitionItemBindingImpl extends DefinitionItemBinding implements com.example.android.definitions.generated.callback.OnClickListener.Listener {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.left_well, 8);
        sViewsWithIds.put(R.id.right_well, 9);
    }
    // views
    @NonNull
    private final com.google.android.material.card.MaterialCardView mboundView0;
    // variables
    @Nullable
    private final android.view.View.OnClickListener mCallback4;
    // values
    // listeners
    // Inverse Binding Event Handlers

    public DefinitionItemBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 10, sIncludes, sViewsWithIds));
    }
    private DefinitionItemBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (android.widget.TextView) bindings[4]
            , (android.view.View) bindings[7]
            , (android.widget.TextView) bindings[2]
            , (android.widget.TextView) bindings[3]
            , (androidx.constraintlayout.widget.Guideline) bindings[8]
            , (androidx.constraintlayout.widget.Guideline) bindings[9]
            , (android.widget.TextView) bindings[6]
            , (android.widget.TextView) bindings[5]
            , (android.widget.TextView) bindings[1]
            );
        this.author.setTag(null);
        this.clickableOverlay.setTag(null);
        this.definitionText.setTag(null);
        this.example.setTag(null);
        this.mboundView0 = (com.google.android.material.card.MaterialCardView) bindings[0];
        this.mboundView0.setTag(null);
        this.thumbsDown.setTag(null);
        this.thumbsUp.setTag(null);
        this.word.setTag(null);
        setRootTag(root);
        // listeners
        mCallback4 = new com.example.android.definitions.generated.callback.OnClickListener(this, 1);
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x4L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.definitionCallback == variableId) {
            setDefinitionCallback((com.example.android.definitions.ui.DefinitionClick) variable);
        }
        else if (BR.definition == variableId) {
            setDefinition((com.example.android.definitions.domain.Definition) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setDefinitionCallback(@Nullable com.example.android.definitions.ui.DefinitionClick DefinitionCallback) {
        this.mDefinitionCallback = DefinitionCallback;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.definitionCallback);
        super.requestRebind();
    }
    public void setDefinition(@Nullable com.example.android.definitions.domain.Definition Definition) {
        this.mDefinition = Definition;
        synchronized(this) {
            mDirtyFlags |= 0x2L;
        }
        notifyPropertyChanged(BR.definition);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        int androidxDatabindingViewDataBindingSafeUnboxDefinitionThumbsUp = 0;
        java.lang.String definitionWord = null;
        java.lang.String stringValueOfDefinitionThumbsDown = null;
        java.lang.String stringValueOfDefinitionThumbsUp = null;
        int androidxDatabindingViewDataBindingSafeUnboxDefinitionThumbsDown = 0;
        java.lang.String definitionAuthor = null;
        java.lang.Integer definitionThumbsUp = null;
        com.example.android.definitions.ui.DefinitionClick definitionCallback = mDefinitionCallback;
        java.lang.String definitionExample = null;
        java.lang.String definitionDefinition = null;
        com.example.android.definitions.domain.Definition definition = mDefinition;
        java.lang.Integer definitionThumbsDown = null;

        if ((dirtyFlags & 0x6L) != 0) {



                if (definition != null) {
                    // read definition.word
                    definitionWord = definition.getWord();
                    // read definition.author
                    definitionAuthor = definition.getAuthor();
                    // read definition.thumbsUp
                    definitionThumbsUp = definition.getThumbsUp();
                    // read definition.example
                    definitionExample = definition.getExample();
                    // read definition.definition
                    definitionDefinition = definition.getDefinition();
                    // read definition.thumbsDown
                    definitionThumbsDown = definition.getThumbsDown();
                }


                // read androidx.databinding.ViewDataBinding.safeUnbox(definition.thumbsUp)
                androidxDatabindingViewDataBindingSafeUnboxDefinitionThumbsUp = androidx.databinding.ViewDataBinding.safeUnbox(definitionThumbsUp);
                // read androidx.databinding.ViewDataBinding.safeUnbox(definition.thumbsDown)
                androidxDatabindingViewDataBindingSafeUnboxDefinitionThumbsDown = androidx.databinding.ViewDataBinding.safeUnbox(definitionThumbsDown);


                // read String.valueOf(androidx.databinding.ViewDataBinding.safeUnbox(definition.thumbsUp))
                stringValueOfDefinitionThumbsUp = java.lang.String.valueOf(androidxDatabindingViewDataBindingSafeUnboxDefinitionThumbsUp);
                // read String.valueOf(androidx.databinding.ViewDataBinding.safeUnbox(definition.thumbsDown))
                stringValueOfDefinitionThumbsDown = java.lang.String.valueOf(androidxDatabindingViewDataBindingSafeUnboxDefinitionThumbsDown);
        }
        // batch finished
        if ((dirtyFlags & 0x6L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.author, definitionAuthor);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.definitionText, definitionDefinition);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.example, definitionExample);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.thumbsDown, stringValueOfDefinitionThumbsDown);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.thumbsUp, stringValueOfDefinitionThumbsUp);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.word, definitionWord);
        }
        if ((dirtyFlags & 0x4L) != 0) {
            // api target 1

            this.clickableOverlay.setOnClickListener(mCallback4);
        }
    }
    // Listener Stub Implementations
    // callback impls
    public final void _internalCallbackOnClick(int sourceId , android.view.View callbackArg_0) {
        // localize variables for thread safety
        // definition
        com.example.android.definitions.domain.Definition definition = mDefinition;
        // definitionCallback != null
        boolean definitionCallbackJavaLangObjectNull = false;
        // definitionCallback
        com.example.android.definitions.ui.DefinitionClick definitionCallback = mDefinitionCallback;



        definitionCallbackJavaLangObjectNull = (definitionCallback) != (null);
        if (definitionCallbackJavaLangObjectNull) {



            definitionCallback.onClick(definition);
        }
    }
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): definitionCallback
        flag 1 (0x2L): definition
        flag 2 (0x3L): null
    flag mapping end*/
    //end
}