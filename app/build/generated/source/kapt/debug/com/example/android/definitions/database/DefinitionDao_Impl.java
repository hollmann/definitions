package com.example.android.definitions.database;

import android.database.Cursor;
import androidx.room.EntityInsertionAdapter;
import androidx.room.RoomDatabase;
import androidx.room.RoomSQLiteQuery;
import androidx.room.util.CursorUtil;
import androidx.room.util.DBUtil;
import androidx.sqlite.db.SupportSQLiteStatement;
import java.lang.Integer;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings({"unchecked", "deprecation"})
public final class DefinitionDao_Impl implements DefinitionDao {
  private final RoomDatabase __db;

  private final EntityInsertionAdapter<DatabaseDefinition> __insertionAdapterOfDatabaseDefinition;

  public DefinitionDao_Impl(RoomDatabase __db) {
    this.__db = __db;
    this.__insertionAdapterOfDatabaseDefinition = new EntityInsertionAdapter<DatabaseDefinition>(__db) {
      @Override
      public String createQuery() {
        return "INSERT OR REPLACE INTO `DatabaseDefinition` (`defid`,`definition`,`word`,`thumbs_up`,`thumbs_down`,`writtenOn`,`author`,`example`,`searched_word`) VALUES (?,?,?,?,?,?,?,?,?)";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, DatabaseDefinition value) {
        if (value.getDefid() == null) {
          stmt.bindNull(1);
        } else {
          stmt.bindLong(1, value.getDefid());
        }
        if (value.getDefinition() == null) {
          stmt.bindNull(2);
        } else {
          stmt.bindString(2, value.getDefinition());
        }
        if (value.getWord() == null) {
          stmt.bindNull(3);
        } else {
          stmt.bindString(3, value.getWord());
        }
        if (value.getThumbsUp() == null) {
          stmt.bindNull(4);
        } else {
          stmt.bindLong(4, value.getThumbsUp());
        }
        if (value.getThumbsDown() == null) {
          stmt.bindNull(5);
        } else {
          stmt.bindLong(5, value.getThumbsDown());
        }
        if (value.getWrittenOn() == null) {
          stmt.bindNull(6);
        } else {
          stmt.bindString(6, value.getWrittenOn());
        }
        if (value.getAuthor() == null) {
          stmt.bindNull(7);
        } else {
          stmt.bindString(7, value.getAuthor());
        }
        if (value.getExample() == null) {
          stmt.bindNull(8);
        } else {
          stmt.bindString(8, value.getExample());
        }
        if (value.getSearchedWord() == null) {
          stmt.bindNull(9);
        } else {
          stmt.bindString(9, value.getSearchedWord());
        }
      }
    };
  }

  @Override
  public void insertAll(final List<DatabaseDefinition> definitions) {
    __db.assertNotSuspendingTransaction();
    __db.beginTransaction();
    try {
      __insertionAdapterOfDatabaseDefinition.insert(definitions);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public List<DatabaseDefinition> getDefinitions(final String searchedWord) {
    final String _sql = "select `DatabaseDefinition`.`defid` AS `defid`, `DatabaseDefinition`.`definition` AS `definition`, `DatabaseDefinition`.`word` AS `word`, `DatabaseDefinition`.`thumbs_up` AS `thumbs_up`, `DatabaseDefinition`.`thumbs_down` AS `thumbs_down`, `DatabaseDefinition`.`writtenOn` AS `writtenOn`, `DatabaseDefinition`.`author` AS `author`, `DatabaseDefinition`.`example` AS `example`, `DatabaseDefinition`.`searched_word` AS `searched_word` from databasedefinition where searched_word like ? order by thumbs_up desc";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 1);
    int _argIndex = 1;
    if (searchedWord == null) {
      _statement.bindNull(_argIndex);
    } else {
      _statement.bindString(_argIndex, searchedWord);
    }
    __db.assertNotSuspendingTransaction();
    final Cursor _cursor = DBUtil.query(__db, _statement, false, null);
    try {
      final int _cursorIndexOfDefid = CursorUtil.getColumnIndexOrThrow(_cursor, "defid");
      final int _cursorIndexOfDefinition = CursorUtil.getColumnIndexOrThrow(_cursor, "definition");
      final int _cursorIndexOfWord = CursorUtil.getColumnIndexOrThrow(_cursor, "word");
      final int _cursorIndexOfThumbsUp = CursorUtil.getColumnIndexOrThrow(_cursor, "thumbs_up");
      final int _cursorIndexOfThumbsDown = CursorUtil.getColumnIndexOrThrow(_cursor, "thumbs_down");
      final int _cursorIndexOfWrittenOn = CursorUtil.getColumnIndexOrThrow(_cursor, "writtenOn");
      final int _cursorIndexOfAuthor = CursorUtil.getColumnIndexOrThrow(_cursor, "author");
      final int _cursorIndexOfExample = CursorUtil.getColumnIndexOrThrow(_cursor, "example");
      final int _cursorIndexOfSearchedWord = CursorUtil.getColumnIndexOrThrow(_cursor, "searched_word");
      final List<DatabaseDefinition> _result = new ArrayList<DatabaseDefinition>(_cursor.getCount());
      while(_cursor.moveToNext()) {
        final DatabaseDefinition _item;
        final Integer _tmpDefid;
        if (_cursor.isNull(_cursorIndexOfDefid)) {
          _tmpDefid = null;
        } else {
          _tmpDefid = _cursor.getInt(_cursorIndexOfDefid);
        }
        final String _tmpDefinition;
        _tmpDefinition = _cursor.getString(_cursorIndexOfDefinition);
        final String _tmpWord;
        _tmpWord = _cursor.getString(_cursorIndexOfWord);
        final Integer _tmpThumbsUp;
        if (_cursor.isNull(_cursorIndexOfThumbsUp)) {
          _tmpThumbsUp = null;
        } else {
          _tmpThumbsUp = _cursor.getInt(_cursorIndexOfThumbsUp);
        }
        final Integer _tmpThumbsDown;
        if (_cursor.isNull(_cursorIndexOfThumbsDown)) {
          _tmpThumbsDown = null;
        } else {
          _tmpThumbsDown = _cursor.getInt(_cursorIndexOfThumbsDown);
        }
        final String _tmpWrittenOn;
        _tmpWrittenOn = _cursor.getString(_cursorIndexOfWrittenOn);
        final String _tmpAuthor;
        _tmpAuthor = _cursor.getString(_cursorIndexOfAuthor);
        final String _tmpExample;
        _tmpExample = _cursor.getString(_cursorIndexOfExample);
        final String _tmpSearchedWord;
        _tmpSearchedWord = _cursor.getString(_cursorIndexOfSearchedWord);
        _item = new DatabaseDefinition(_tmpDefid,_tmpDefinition,_tmpWord,_tmpThumbsUp,_tmpThumbsDown,_tmpWrittenOn,_tmpAuthor,_tmpExample,_tmpSearchedWord);
        _result.add(_item);
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }

  @Override
  public List<DatabaseDefinition> getDefinitionsOrderyByThumbsUp(final String searchedWord) {
    final String _sql = "select `DatabaseDefinition`.`defid` AS `defid`, `DatabaseDefinition`.`definition` AS `definition`, `DatabaseDefinition`.`word` AS `word`, `DatabaseDefinition`.`thumbs_up` AS `thumbs_up`, `DatabaseDefinition`.`thumbs_down` AS `thumbs_down`, `DatabaseDefinition`.`writtenOn` AS `writtenOn`, `DatabaseDefinition`.`author` AS `author`, `DatabaseDefinition`.`example` AS `example`, `DatabaseDefinition`.`searched_word` AS `searched_word` from databasedefinition where searched_word like ? order by thumbs_up desc";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 1);
    int _argIndex = 1;
    if (searchedWord == null) {
      _statement.bindNull(_argIndex);
    } else {
      _statement.bindString(_argIndex, searchedWord);
    }
    __db.assertNotSuspendingTransaction();
    final Cursor _cursor = DBUtil.query(__db, _statement, false, null);
    try {
      final int _cursorIndexOfDefid = CursorUtil.getColumnIndexOrThrow(_cursor, "defid");
      final int _cursorIndexOfDefinition = CursorUtil.getColumnIndexOrThrow(_cursor, "definition");
      final int _cursorIndexOfWord = CursorUtil.getColumnIndexOrThrow(_cursor, "word");
      final int _cursorIndexOfThumbsUp = CursorUtil.getColumnIndexOrThrow(_cursor, "thumbs_up");
      final int _cursorIndexOfThumbsDown = CursorUtil.getColumnIndexOrThrow(_cursor, "thumbs_down");
      final int _cursorIndexOfWrittenOn = CursorUtil.getColumnIndexOrThrow(_cursor, "writtenOn");
      final int _cursorIndexOfAuthor = CursorUtil.getColumnIndexOrThrow(_cursor, "author");
      final int _cursorIndexOfExample = CursorUtil.getColumnIndexOrThrow(_cursor, "example");
      final int _cursorIndexOfSearchedWord = CursorUtil.getColumnIndexOrThrow(_cursor, "searched_word");
      final List<DatabaseDefinition> _result = new ArrayList<DatabaseDefinition>(_cursor.getCount());
      while(_cursor.moveToNext()) {
        final DatabaseDefinition _item;
        final Integer _tmpDefid;
        if (_cursor.isNull(_cursorIndexOfDefid)) {
          _tmpDefid = null;
        } else {
          _tmpDefid = _cursor.getInt(_cursorIndexOfDefid);
        }
        final String _tmpDefinition;
        _tmpDefinition = _cursor.getString(_cursorIndexOfDefinition);
        final String _tmpWord;
        _tmpWord = _cursor.getString(_cursorIndexOfWord);
        final Integer _tmpThumbsUp;
        if (_cursor.isNull(_cursorIndexOfThumbsUp)) {
          _tmpThumbsUp = null;
        } else {
          _tmpThumbsUp = _cursor.getInt(_cursorIndexOfThumbsUp);
        }
        final Integer _tmpThumbsDown;
        if (_cursor.isNull(_cursorIndexOfThumbsDown)) {
          _tmpThumbsDown = null;
        } else {
          _tmpThumbsDown = _cursor.getInt(_cursorIndexOfThumbsDown);
        }
        final String _tmpWrittenOn;
        _tmpWrittenOn = _cursor.getString(_cursorIndexOfWrittenOn);
        final String _tmpAuthor;
        _tmpAuthor = _cursor.getString(_cursorIndexOfAuthor);
        final String _tmpExample;
        _tmpExample = _cursor.getString(_cursorIndexOfExample);
        final String _tmpSearchedWord;
        _tmpSearchedWord = _cursor.getString(_cursorIndexOfSearchedWord);
        _item = new DatabaseDefinition(_tmpDefid,_tmpDefinition,_tmpWord,_tmpThumbsUp,_tmpThumbsDown,_tmpWrittenOn,_tmpAuthor,_tmpExample,_tmpSearchedWord);
        _result.add(_item);
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }

  @Override
  public List<DatabaseDefinition> getDefinitionsOrderyByThumbsDown(final String searchedWord) {
    final String _sql = "select `DatabaseDefinition`.`defid` AS `defid`, `DatabaseDefinition`.`definition` AS `definition`, `DatabaseDefinition`.`word` AS `word`, `DatabaseDefinition`.`thumbs_up` AS `thumbs_up`, `DatabaseDefinition`.`thumbs_down` AS `thumbs_down`, `DatabaseDefinition`.`writtenOn` AS `writtenOn`, `DatabaseDefinition`.`author` AS `author`, `DatabaseDefinition`.`example` AS `example`, `DatabaseDefinition`.`searched_word` AS `searched_word` from databasedefinition where searched_word like ? order by thumbs_down desc";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 1);
    int _argIndex = 1;
    if (searchedWord == null) {
      _statement.bindNull(_argIndex);
    } else {
      _statement.bindString(_argIndex, searchedWord);
    }
    __db.assertNotSuspendingTransaction();
    final Cursor _cursor = DBUtil.query(__db, _statement, false, null);
    try {
      final int _cursorIndexOfDefid = CursorUtil.getColumnIndexOrThrow(_cursor, "defid");
      final int _cursorIndexOfDefinition = CursorUtil.getColumnIndexOrThrow(_cursor, "definition");
      final int _cursorIndexOfWord = CursorUtil.getColumnIndexOrThrow(_cursor, "word");
      final int _cursorIndexOfThumbsUp = CursorUtil.getColumnIndexOrThrow(_cursor, "thumbs_up");
      final int _cursorIndexOfThumbsDown = CursorUtil.getColumnIndexOrThrow(_cursor, "thumbs_down");
      final int _cursorIndexOfWrittenOn = CursorUtil.getColumnIndexOrThrow(_cursor, "writtenOn");
      final int _cursorIndexOfAuthor = CursorUtil.getColumnIndexOrThrow(_cursor, "author");
      final int _cursorIndexOfExample = CursorUtil.getColumnIndexOrThrow(_cursor, "example");
      final int _cursorIndexOfSearchedWord = CursorUtil.getColumnIndexOrThrow(_cursor, "searched_word");
      final List<DatabaseDefinition> _result = new ArrayList<DatabaseDefinition>(_cursor.getCount());
      while(_cursor.moveToNext()) {
        final DatabaseDefinition _item;
        final Integer _tmpDefid;
        if (_cursor.isNull(_cursorIndexOfDefid)) {
          _tmpDefid = null;
        } else {
          _tmpDefid = _cursor.getInt(_cursorIndexOfDefid);
        }
        final String _tmpDefinition;
        _tmpDefinition = _cursor.getString(_cursorIndexOfDefinition);
        final String _tmpWord;
        _tmpWord = _cursor.getString(_cursorIndexOfWord);
        final Integer _tmpThumbsUp;
        if (_cursor.isNull(_cursorIndexOfThumbsUp)) {
          _tmpThumbsUp = null;
        } else {
          _tmpThumbsUp = _cursor.getInt(_cursorIndexOfThumbsUp);
        }
        final Integer _tmpThumbsDown;
        if (_cursor.isNull(_cursorIndexOfThumbsDown)) {
          _tmpThumbsDown = null;
        } else {
          _tmpThumbsDown = _cursor.getInt(_cursorIndexOfThumbsDown);
        }
        final String _tmpWrittenOn;
        _tmpWrittenOn = _cursor.getString(_cursorIndexOfWrittenOn);
        final String _tmpAuthor;
        _tmpAuthor = _cursor.getString(_cursorIndexOfAuthor);
        final String _tmpExample;
        _tmpExample = _cursor.getString(_cursorIndexOfExample);
        final String _tmpSearchedWord;
        _tmpSearchedWord = _cursor.getString(_cursorIndexOfSearchedWord);
        _item = new DatabaseDefinition(_tmpDefid,_tmpDefinition,_tmpWord,_tmpThumbsUp,_tmpThumbsDown,_tmpWrittenOn,_tmpAuthor,_tmpExample,_tmpSearchedWord);
        _result.add(_item);
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }
}
