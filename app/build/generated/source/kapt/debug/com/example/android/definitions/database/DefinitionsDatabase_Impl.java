package com.example.android.definitions.database;

import androidx.room.DatabaseConfiguration;
import androidx.room.InvalidationTracker;
import androidx.room.RoomOpenHelper;
import androidx.room.RoomOpenHelper.Delegate;
import androidx.room.RoomOpenHelper.ValidationResult;
import androidx.room.util.DBUtil;
import androidx.room.util.TableInfo;
import androidx.room.util.TableInfo.Column;
import androidx.room.util.TableInfo.ForeignKey;
import androidx.room.util.TableInfo.Index;
import androidx.sqlite.db.SupportSQLiteDatabase;
import androidx.sqlite.db.SupportSQLiteOpenHelper;
import androidx.sqlite.db.SupportSQLiteOpenHelper.Callback;
import androidx.sqlite.db.SupportSQLiteOpenHelper.Configuration;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

@SuppressWarnings({"unchecked", "deprecation"})
public final class DefinitionsDatabase_Impl extends DefinitionsDatabase {
  private volatile DefinitionDao _definitionDao;

  @Override
  protected SupportSQLiteOpenHelper createOpenHelper(DatabaseConfiguration configuration) {
    final SupportSQLiteOpenHelper.Callback _openCallback = new RoomOpenHelper(configuration, new RoomOpenHelper.Delegate(1) {
      @Override
      public void createAllTables(SupportSQLiteDatabase _db) {
        _db.execSQL("CREATE TABLE IF NOT EXISTS `DatabaseDefinition` (`defid` INTEGER NOT NULL, `definition` TEXT NOT NULL, `word` TEXT NOT NULL, `thumbs_up` INTEGER NOT NULL, `thumbs_down` INTEGER NOT NULL, `writtenOn` TEXT NOT NULL, `author` TEXT NOT NULL, `example` TEXT NOT NULL, `searched_word` TEXT NOT NULL, PRIMARY KEY(`defid`))");
        _db.execSQL("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
        _db.execSQL("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, 'c0ae478e61f02dcfbdb81880755be96f')");
      }

      @Override
      public void dropAllTables(SupportSQLiteDatabase _db) {
        _db.execSQL("DROP TABLE IF EXISTS `DatabaseDefinition`");
        if (mCallbacks != null) {
          for (int _i = 0, _size = mCallbacks.size(); _i < _size; _i++) {
            mCallbacks.get(_i).onDestructiveMigration(_db);
          }
        }
      }

      @Override
      protected void onCreate(SupportSQLiteDatabase _db) {
        if (mCallbacks != null) {
          for (int _i = 0, _size = mCallbacks.size(); _i < _size; _i++) {
            mCallbacks.get(_i).onCreate(_db);
          }
        }
      }

      @Override
      public void onOpen(SupportSQLiteDatabase _db) {
        mDatabase = _db;
        internalInitInvalidationTracker(_db);
        if (mCallbacks != null) {
          for (int _i = 0, _size = mCallbacks.size(); _i < _size; _i++) {
            mCallbacks.get(_i).onOpen(_db);
          }
        }
      }

      @Override
      public void onPreMigrate(SupportSQLiteDatabase _db) {
        DBUtil.dropFtsSyncTriggers(_db);
      }

      @Override
      public void onPostMigrate(SupportSQLiteDatabase _db) {
      }

      @Override
      protected RoomOpenHelper.ValidationResult onValidateSchema(SupportSQLiteDatabase _db) {
        final HashMap<String, TableInfo.Column> _columnsDatabaseDefinition = new HashMap<String, TableInfo.Column>(9);
        _columnsDatabaseDefinition.put("defid", new TableInfo.Column("defid", "INTEGER", true, 1, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsDatabaseDefinition.put("definition", new TableInfo.Column("definition", "TEXT", true, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsDatabaseDefinition.put("word", new TableInfo.Column("word", "TEXT", true, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsDatabaseDefinition.put("thumbs_up", new TableInfo.Column("thumbs_up", "INTEGER", true, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsDatabaseDefinition.put("thumbs_down", new TableInfo.Column("thumbs_down", "INTEGER", true, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsDatabaseDefinition.put("writtenOn", new TableInfo.Column("writtenOn", "TEXT", true, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsDatabaseDefinition.put("author", new TableInfo.Column("author", "TEXT", true, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsDatabaseDefinition.put("example", new TableInfo.Column("example", "TEXT", true, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsDatabaseDefinition.put("searched_word", new TableInfo.Column("searched_word", "TEXT", true, 0, null, TableInfo.CREATED_FROM_ENTITY));
        final HashSet<TableInfo.ForeignKey> _foreignKeysDatabaseDefinition = new HashSet<TableInfo.ForeignKey>(0);
        final HashSet<TableInfo.Index> _indicesDatabaseDefinition = new HashSet<TableInfo.Index>(0);
        final TableInfo _infoDatabaseDefinition = new TableInfo("DatabaseDefinition", _columnsDatabaseDefinition, _foreignKeysDatabaseDefinition, _indicesDatabaseDefinition);
        final TableInfo _existingDatabaseDefinition = TableInfo.read(_db, "DatabaseDefinition");
        if (! _infoDatabaseDefinition.equals(_existingDatabaseDefinition)) {
          return new RoomOpenHelper.ValidationResult(false, "DatabaseDefinition(com.example.android.definitions.database.DatabaseDefinition).\n"
                  + " Expected:\n" + _infoDatabaseDefinition + "\n"
                  + " Found:\n" + _existingDatabaseDefinition);
        }
        return new RoomOpenHelper.ValidationResult(true, null);
      }
    }, "c0ae478e61f02dcfbdb81880755be96f", "c7ec061f4afb88689a7bdb96cf0fff0c");
    final SupportSQLiteOpenHelper.Configuration _sqliteConfig = SupportSQLiteOpenHelper.Configuration.builder(configuration.context)
        .name(configuration.name)
        .callback(_openCallback)
        .build();
    final SupportSQLiteOpenHelper _helper = configuration.sqliteOpenHelperFactory.create(_sqliteConfig);
    return _helper;
  }

  @Override
  protected InvalidationTracker createInvalidationTracker() {
    final HashMap<String, String> _shadowTablesMap = new HashMap<String, String>(0);
    HashMap<String, Set<String>> _viewTables = new HashMap<String, Set<String>>(0);
    return new InvalidationTracker(this, _shadowTablesMap, _viewTables, "DatabaseDefinition");
  }

  @Override
  public void clearAllTables() {
    super.assertNotMainThread();
    final SupportSQLiteDatabase _db = super.getOpenHelper().getWritableDatabase();
    try {
      super.beginTransaction();
      _db.execSQL("DELETE FROM `DatabaseDefinition`");
      super.setTransactionSuccessful();
    } finally {
      super.endTransaction();
      _db.query("PRAGMA wal_checkpoint(FULL)").close();
      if (!_db.inTransaction()) {
        _db.execSQL("VACUUM");
      }
    }
  }

  @Override
  public DefinitionDao getDefinitionDao() {
    if (_definitionDao != null) {
      return _definitionDao;
    } else {
      synchronized(this) {
        if(_definitionDao == null) {
          _definitionDao = new DefinitionDao_Impl(this);
        }
        return _definitionDao;
      }
    }
  }
}
