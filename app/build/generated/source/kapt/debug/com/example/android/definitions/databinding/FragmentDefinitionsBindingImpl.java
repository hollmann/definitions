package com.example.android.definitions.databinding;
import com.example.android.definitions.R;
import com.example.android.definitions.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class FragmentDefinitionsBindingImpl extends FragmentDefinitionsBinding implements com.example.android.definitions.generated.callback.OnClickListener.Listener {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.order_by_radio_group, 7);
    }
    // views
    @NonNull
    private final androidx.constraintlayout.widget.ConstraintLayout mboundView0;
    // variables
    @Nullable
    private final android.view.View.OnClickListener mCallback2;
    @Nullable
    private final android.view.View.OnClickListener mCallback3;
    @Nullable
    private final android.view.View.OnClickListener mCallback1;
    // values
    // listeners
    // Inverse Binding Event Handlers
    private androidx.databinding.InverseBindingListener wordEditextandroidTextAttrChanged = new androidx.databinding.InverseBindingListener() {
        @Override
        public void onChange() {
            // Inverse of viewModel.searchWord.getValue()
            //         is viewModel.searchWord.setValue((java.lang.String) callbackArg_0)
            java.lang.String callbackArg_0 = androidx.databinding.adapters.TextViewBindingAdapter.getTextString(wordEditext);
            // localize variables for thread safety
            // viewModel.searchWord
            androidx.lifecycle.MutableLiveData<java.lang.String> viewModelSearchWord = null;
            // viewModel.searchWord != null
            boolean viewModelSearchWordJavaLangObjectNull = false;
            // viewModel
            com.example.android.definitions.viewmodels.DefinitionsViewModel viewModel = mViewModel;
            // viewModel != null
            boolean viewModelJavaLangObjectNull = false;
            // viewModel.searchWord.getValue()
            java.lang.String viewModelSearchWordGetValue = null;



            viewModelJavaLangObjectNull = (viewModel) != (null);
            if (viewModelJavaLangObjectNull) {


                viewModelSearchWord = viewModel.getSearchWord();

                viewModelSearchWordJavaLangObjectNull = (viewModelSearchWord) != (null);
                if (viewModelSearchWordJavaLangObjectNull) {




                    viewModelSearchWord.setValue(((java.lang.String) (callbackArg_0)));
                }
            }
        }
    };

    public FragmentDefinitionsBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 8, sIncludes, sViewsWithIds));
    }
    private FragmentDefinitionsBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 4
            , (android.widget.ProgressBar) bindings[6]
            , (android.widget.RadioGroup) bindings[7]
            , (android.widget.RadioButton) bindings[4]
            , (android.widget.RadioButton) bindings[3]
            , (androidx.recyclerview.widget.RecyclerView) bindings[5]
            , (android.widget.TextView) bindings[2]
            , (android.widget.EditText) bindings[1]
            );
        this.loadingSpinner.setTag(null);
        this.mboundView0 = (androidx.constraintlayout.widget.ConstraintLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.orderByThumbsDown.setTag("thumbs_down");
        this.orderByThumbsUp.setTag("thumbs_up");
        this.recyclerView.setTag(null);
        this.search.setTag(null);
        this.wordEditext.setTag(null);
        setRootTag(root);
        // listeners
        mCallback2 = new com.example.android.definitions.generated.callback.OnClickListener(this, 2);
        mCallback3 = new com.example.android.definitions.generated.callback.OnClickListener(this, 3);
        mCallback1 = new com.example.android.definitions.generated.callback.OnClickListener(this, 1);
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x20L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.viewModel == variableId) {
            setViewModel((com.example.android.definitions.viewmodels.DefinitionsViewModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setViewModel(@Nullable com.example.android.definitions.viewmodels.DefinitionsViewModel ViewModel) {
        this.mViewModel = ViewModel;
        synchronized(this) {
            mDirtyFlags |= 0x10L;
        }
        notifyPropertyChanged(BR.viewModel);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeViewModelIsLoading((androidx.lifecycle.LiveData<java.lang.Boolean>) object, fieldId);
            case 1 :
                return onChangeViewModelEventNetworkError((androidx.lifecycle.LiveData<java.lang.Boolean>) object, fieldId);
            case 2 :
                return onChangeViewModelCheckedRadioButton((androidx.lifecycle.MutableLiveData<com.example.android.definitions.repository.DefinitionsRepository.SortBy>) object, fieldId);
            case 3 :
                return onChangeViewModelSearchWord((androidx.lifecycle.MutableLiveData<java.lang.String>) object, fieldId);
        }
        return false;
    }
    private boolean onChangeViewModelIsLoading(androidx.lifecycle.LiveData<java.lang.Boolean> ViewModelIsLoading, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelEventNetworkError(androidx.lifecycle.LiveData<java.lang.Boolean> ViewModelEventNetworkError, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x2L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelCheckedRadioButton(androidx.lifecycle.MutableLiveData<com.example.android.definitions.repository.DefinitionsRepository.SortBy> ViewModelCheckedRadioButton, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x4L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelSearchWord(androidx.lifecycle.MutableLiveData<java.lang.String> ViewModelSearchWord, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x8L;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        androidx.lifecycle.LiveData<java.lang.Boolean> viewModelIsLoading = null;
        com.example.android.definitions.repository.DefinitionsRepository.SortBy viewModelCheckedRadioButtonGetValue = null;
        boolean androidxDatabindingViewDataBindingSafeUnboxViewModelEventNetworkError = false;
        boolean viewModelCheckedRadioButtonSortByTHUMBSUP = false;
        boolean viewModelCheckedRadioButtonSortByTHUMBSDOWN = false;
        java.lang.String viewModelSearchWordGetValue = null;
        androidx.lifecycle.LiveData<java.lang.Boolean> viewModelEventNetworkError = null;
        java.lang.Boolean viewModelIsLoadingGetValue = null;
        androidx.lifecycle.MutableLiveData<com.example.android.definitions.repository.DefinitionsRepository.SortBy> viewModelCheckedRadioButton = null;
        androidx.lifecycle.MutableLiveData<java.lang.String> viewModelSearchWord = null;
        java.lang.Boolean viewModelEventNetworkErrorGetValue = null;
        com.example.android.definitions.viewmodels.DefinitionsViewModel viewModel = mViewModel;

        if ((dirtyFlags & 0x3fL) != 0) {


            if ((dirtyFlags & 0x31L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.isLoading()
                        viewModelIsLoading = viewModel.isLoading();
                    }
                    updateLiveDataRegistration(0, viewModelIsLoading);


                    if (viewModelIsLoading != null) {
                        // read viewModel.isLoading().getValue()
                        viewModelIsLoadingGetValue = viewModelIsLoading.getValue();
                    }
            }
            if ((dirtyFlags & 0x32L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.eventNetworkError
                        viewModelEventNetworkError = viewModel.getEventNetworkError();
                    }
                    updateLiveDataRegistration(1, viewModelEventNetworkError);


                    if (viewModelEventNetworkError != null) {
                        // read viewModel.eventNetworkError.getValue()
                        viewModelEventNetworkErrorGetValue = viewModelEventNetworkError.getValue();
                    }


                    // read androidx.databinding.ViewDataBinding.safeUnbox(viewModel.eventNetworkError.getValue())
                    androidxDatabindingViewDataBindingSafeUnboxViewModelEventNetworkError = androidx.databinding.ViewDataBinding.safeUnbox(viewModelEventNetworkErrorGetValue);
            }
            if ((dirtyFlags & 0x34L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.checkedRadioButton
                        viewModelCheckedRadioButton = viewModel.getCheckedRadioButton();
                    }
                    updateLiveDataRegistration(2, viewModelCheckedRadioButton);


                    if (viewModelCheckedRadioButton != null) {
                        // read viewModel.checkedRadioButton.getValue()
                        viewModelCheckedRadioButtonGetValue = viewModelCheckedRadioButton.getValue();
                    }


                    // read viewModel.checkedRadioButton.getValue() == SortBy.THUMBS_UP
                    viewModelCheckedRadioButtonSortByTHUMBSUP = (viewModelCheckedRadioButtonGetValue) == (com.example.android.definitions.repository.DefinitionsRepository.SortBy.THUMBS_UP);
                    // read viewModel.checkedRadioButton.getValue() == SortBy.THUMBS_DOWN
                    viewModelCheckedRadioButtonSortByTHUMBSDOWN = (viewModelCheckedRadioButtonGetValue) == (com.example.android.definitions.repository.DefinitionsRepository.SortBy.THUMBS_DOWN);
            }
            if ((dirtyFlags & 0x38L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.searchWord
                        viewModelSearchWord = viewModel.getSearchWord();
                    }
                    updateLiveDataRegistration(3, viewModelSearchWord);


                    if (viewModelSearchWord != null) {
                        // read viewModel.searchWord.getValue()
                        viewModelSearchWordGetValue = viewModelSearchWord.getValue();
                    }
            }
        }
        // batch finished
        if ((dirtyFlags & 0x31L) != 0) {
            // api target 1

            com.example.android.definitions.util.BindingAdaptersKt.displayWhenLoading(this.loadingSpinner, viewModelIsLoadingGetValue);
            com.example.android.definitions.util.BindingAdaptersKt.hideWhenLoading(this.recyclerView, viewModelIsLoadingGetValue);
        }
        if ((dirtyFlags & 0x32L) != 0) {
            // api target 1

            com.example.android.definitions.util.BindingAdaptersKt.hideIfNetworkError(this.loadingSpinner, androidxDatabindingViewDataBindingSafeUnboxViewModelEventNetworkError);
        }
        if ((dirtyFlags & 0x34L) != 0) {
            // api target 1

            androidx.databinding.adapters.CompoundButtonBindingAdapter.setChecked(this.orderByThumbsDown, viewModelCheckedRadioButtonSortByTHUMBSDOWN);
            androidx.databinding.adapters.CompoundButtonBindingAdapter.setChecked(this.orderByThumbsUp, viewModelCheckedRadioButtonSortByTHUMBSUP);
        }
        if ((dirtyFlags & 0x20L) != 0) {
            // api target 1

            this.orderByThumbsDown.setOnClickListener(mCallback3);
            this.orderByThumbsUp.setOnClickListener(mCallback2);
            this.search.setOnClickListener(mCallback1);
            androidx.databinding.adapters.TextViewBindingAdapter.setTextWatcher(this.wordEditext, (androidx.databinding.adapters.TextViewBindingAdapter.BeforeTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.OnTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.AfterTextChanged)null, wordEditextandroidTextAttrChanged);
        }
        if ((dirtyFlags & 0x38L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.wordEditext, viewModelSearchWordGetValue);
        }
    }
    // Listener Stub Implementations
    // callback impls
    public final void _internalCallbackOnClick(int sourceId , android.view.View callbackArg_0) {
        switch(sourceId) {
            case 2: {
                // localize variables for thread safety
                // viewModel
                com.example.android.definitions.viewmodels.DefinitionsViewModel viewModel = mViewModel;
                // viewModel != null
                boolean viewModelJavaLangObjectNull = false;



                viewModelJavaLangObjectNull = (viewModel) != (null);
                if (viewModelJavaLangObjectNull) {


                    viewModel.sortByThumbsUp();
                }
                break;
            }
            case 3: {
                // localize variables for thread safety
                // viewModel
                com.example.android.definitions.viewmodels.DefinitionsViewModel viewModel = mViewModel;
                // viewModel != null
                boolean viewModelJavaLangObjectNull = false;



                viewModelJavaLangObjectNull = (viewModel) != (null);
                if (viewModelJavaLangObjectNull) {


                    viewModel.sortByThumbsDown();
                }
                break;
            }
            case 1: {
                // localize variables for thread safety
                // viewModel
                com.example.android.definitions.viewmodels.DefinitionsViewModel viewModel = mViewModel;
                // viewModel != null
                boolean viewModelJavaLangObjectNull = false;



                viewModelJavaLangObjectNull = (viewModel) != (null);
                if (viewModelJavaLangObjectNull) {


                    viewModel.onSearchClicked();
                }
                break;
            }
        }
    }
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): viewModel.isLoading()
        flag 1 (0x2L): viewModel.eventNetworkError
        flag 2 (0x3L): viewModel.checkedRadioButton
        flag 3 (0x4L): viewModel.searchWord
        flag 4 (0x5L): viewModel
        flag 5 (0x6L): null
    flag mapping end*/
    //end
}