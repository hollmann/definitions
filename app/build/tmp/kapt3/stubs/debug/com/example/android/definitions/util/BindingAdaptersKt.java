package com.example.android.definitions.util;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 2, d1 = {"\u0000\u0016\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0005\u001a\u001f\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\b\u0010\u0004\u001a\u0004\u0018\u00010\u0005H\u0007\u00a2\u0006\u0002\u0010\u0006\u001a\u0018\u0010\u0007\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010\b\u001a\u00020\u0005H\u0007\u001a\u001f\u0010\t\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\b\u0010\u0004\u001a\u0004\u0018\u00010\u0005H\u0007\u00a2\u0006\u0002\u0010\u0006\u00a8\u0006\n"}, d2 = {"displayWhenLoading", "", "view", "Landroid/view/View;", "isLoading", "", "(Landroid/view/View;Ljava/lang/Boolean;)V", "hideIfNetworkError", "isNetWorkError", "hideWhenLoading", "app_debug"})
public final class BindingAdaptersKt {
    
    @androidx.databinding.BindingAdapter(value = {"isNetworkError"})
    public static final void hideIfNetworkError(@org.jetbrains.annotations.NotNull()
    android.view.View view, boolean isNetWorkError) {
    }
    
    @androidx.databinding.BindingAdapter(value = {"displayWhenLoading"})
    public static final void displayWhenLoading(@org.jetbrains.annotations.NotNull()
    android.view.View view, @org.jetbrains.annotations.Nullable()
    java.lang.Boolean isLoading) {
    }
    
    @androidx.databinding.BindingAdapter(value = {"hideWhenLoading"})
    public static final void hideWhenLoading(@org.jetbrains.annotations.NotNull()
    android.view.View view, @org.jetbrains.annotations.Nullable()
    java.lang.Boolean isLoading) {
    }
}