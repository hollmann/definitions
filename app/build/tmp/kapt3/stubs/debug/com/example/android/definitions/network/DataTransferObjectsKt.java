package com.example.android.definitions.network;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 2, d1 = {"\u0000\u001c\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\u001a\u0018\u0010\u0000\u001a\b\u0012\u0004\u0012\u00020\u00020\u0001*\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005\u001a\u0010\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00070\u0001*\u00020\u0003\u00a8\u0006\b"}, d2 = {"asDatabaseModel", "", "Lcom/example/android/definitions/database/DatabaseDefinition;", "Lcom/example/android/definitions/network/NetworkDefinitionContainer;", "searchedWord", "", "asDomainModel", "Lcom/example/android/definitions/domain/Definition;", "app_debug"})
public final class DataTransferObjectsKt {
    
    @org.jetbrains.annotations.NotNull()
    public static final java.util.List<com.example.android.definitions.domain.Definition> asDomainModel(@org.jetbrains.annotations.NotNull()
    com.example.android.definitions.network.NetworkDefinitionContainer $this$asDomainModel) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public static final java.util.List<com.example.android.definitions.database.DatabaseDefinition> asDatabaseModel(@org.jetbrains.annotations.NotNull()
    com.example.android.definitions.network.NetworkDefinitionContainer $this$asDatabaseModel, @org.jetbrains.annotations.NotNull()
    java.lang.String searchedWord) {
        return null;
    }
}