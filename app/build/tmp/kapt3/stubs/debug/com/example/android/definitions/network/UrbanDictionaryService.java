package com.example.android.definitions.network;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\bf\u0018\u00002\u00020\u0001J\u001b\u0010\u0002\u001a\u00020\u00032\b\b\u0001\u0010\u0004\u001a\u00020\u0005H\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0006\u0082\u0002\u0004\n\u0002\b\u0019\u00a8\u0006\u0007"}, d2 = {"Lcom/example/android/definitions/network/UrbanDictionaryService;", "", "getDefinition", "Lcom/example/android/definitions/network/NetworkDefinitionContainer;", "word", "", "(Ljava/lang/String;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "app_debug"})
public abstract interface UrbanDictionaryService {
    
    @org.jetbrains.annotations.Nullable()
    @retrofit2.http.GET(value = "define")
    @retrofit2.http.Headers(value = {"x-rapidapi-key: 8c992b37c0msh8335a7b0bd0030dp17be07jsn05e41e7d404d"})
    public abstract java.lang.Object getDefinition(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Query(value = "term")
    java.lang.String word, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super com.example.android.definitions.network.NetworkDefinitionContainer> p1);
}