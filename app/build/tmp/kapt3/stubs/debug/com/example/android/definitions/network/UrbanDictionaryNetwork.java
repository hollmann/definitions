package com.example.android.definitions.network;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u0016\u0010\u0003\u001a\n \u0005*\u0004\u0018\u00010\u00040\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0019\u0010\u0006\u001a\n \u0005*\u0004\u0018\u00010\u00070\u0007\u00a2\u0006\b\n\u0000\u001a\u0004\b\b\u0010\t\u00a8\u0006\n"}, d2 = {"Lcom/example/android/definitions/network/UrbanDictionaryNetwork;", "", "()V", "retrofit", "Lretrofit2/Retrofit;", "kotlin.jvm.PlatformType", "urbanDictionary", "Lcom/example/android/definitions/network/UrbanDictionaryService;", "getUrbanDictionary", "()Lcom/example/android/definitions/network/UrbanDictionaryService;", "app_debug"})
public final class UrbanDictionaryNetwork {
    private static final retrofit2.Retrofit retrofit = null;
    private static final com.example.android.definitions.network.UrbanDictionaryService urbanDictionary = null;
    public static final com.example.android.definitions.network.UrbanDictionaryNetwork INSTANCE = null;
    
    public final com.example.android.definitions.network.UrbanDictionaryService getUrbanDictionary() {
        return null;
    }
    
    private UrbanDictionaryNetwork() {
        super();
    }
}