package com.example.android.definitions.repository;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\u00020\u0001:\u0001\u0016B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0019\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u000eH\u0086@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0012J\u0019\u0010\u0013\u001a\u00020\u00102\u0006\u0010\u0013\u001a\u00020\u0014H\u0086@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0015R\u001a\u0010\u0005\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\b0\u00070\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001d\u0010\t\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\b0\u00070\n8F\u00a2\u0006\u0006\u001a\u0004\b\u000b\u0010\fR\u000e\u0010\r\u001a\u00020\u000eX\u0082\u000e\u00a2\u0006\u0002\n\u0000\u0082\u0002\u0004\n\u0002\b\u0019\u00a8\u0006\u0017"}, d2 = {"Lcom/example/android/definitions/repository/DefinitionsRepository;", "", "database", "Lcom/example/android/definitions/database/DefinitionsDatabase;", "(Lcom/example/android/definitions/database/DefinitionsDatabase;)V", "_definitions", "Landroidx/lifecycle/MutableLiveData;", "", "Lcom/example/android/definitions/domain/Definition;", "definitions", "Landroidx/lifecycle/LiveData;", "getDefinitions", "()Landroidx/lifecycle/LiveData;", "lastSearchedTerm", "", "searchDefinitions", "", "searchedWord", "(Ljava/lang/String;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "sortBy", "Lcom/example/android/definitions/repository/DefinitionsRepository$SortBy;", "(Lcom/example/android/definitions/repository/DefinitionsRepository$SortBy;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "SortBy", "app_debug"})
public final class DefinitionsRepository {
    private java.lang.String lastSearchedTerm = "";
    private final androidx.lifecycle.MutableLiveData<java.util.List<com.example.android.definitions.domain.Definition>> _definitions = null;
    private final com.example.android.definitions.database.DefinitionsDatabase database = null;
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<java.util.List<com.example.android.definitions.domain.Definition>> getDefinitions() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object searchDefinitions(@org.jetbrains.annotations.NotNull()
    java.lang.String searchedWord, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super kotlin.Unit> p1) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object sortBy(@org.jetbrains.annotations.NotNull()
    com.example.android.definitions.repository.DefinitionsRepository.SortBy sortBy, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super kotlin.Unit> p1) {
        return null;
    }
    
    public DefinitionsRepository(@org.jetbrains.annotations.NotNull()
    com.example.android.definitions.database.DefinitionsDatabase database) {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0004\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002j\u0002\b\u0003j\u0002\b\u0004\u00a8\u0006\u0005"}, d2 = {"Lcom/example/android/definitions/repository/DefinitionsRepository$SortBy;", "", "(Ljava/lang/String;I)V", "THUMBS_UP", "THUMBS_DOWN", "app_debug"})
    public static enum SortBy {
        /*public static final*/ THUMBS_UP /* = new THUMBS_UP() */,
        /*public static final*/ THUMBS_DOWN /* = new THUMBS_DOWN() */;
        
        SortBy() {
        }
    }
}