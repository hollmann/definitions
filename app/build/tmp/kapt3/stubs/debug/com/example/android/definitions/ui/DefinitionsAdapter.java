package com.example.android.definitions.ui;

import java.lang.System;

/**
 * As this is a very simple app we can keep the the adapter here. If this grows we
 * will create a new file.
 */
@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005J\b\u0010\u000e\u001a\u00020\u000fH\u0016J\u0018\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u00022\u0006\u0010\u0013\u001a\u00020\u000fH\u0016J\u0018\u0010\u0014\u001a\u00020\u00022\u0006\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u000fH\u0016R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R0\u0010\t\u001a\b\u0012\u0004\u0012\u00020\b0\u00072\f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\b0\u0007@FX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\n\u0010\u000b\"\u0004\b\f\u0010\r\u00a8\u0006\u0018"}, d2 = {"Lcom/example/android/definitions/ui/DefinitionsAdapter;", "Landroidx/recyclerview/widget/RecyclerView$Adapter;", "Lcom/example/android/definitions/ui/DefinitionViewHolder;", "callback", "Lcom/example/android/definitions/ui/DefinitionClick;", "(Lcom/example/android/definitions/ui/DefinitionClick;)V", "value", "", "Lcom/example/android/definitions/domain/Definition;", "definitions", "getDefinitions", "()Ljava/util/List;", "setDefinitions", "(Ljava/util/List;)V", "getItemCount", "", "onBindViewHolder", "", "holder", "position", "onCreateViewHolder", "parent", "Landroid/view/ViewGroup;", "viewType", "app_debug"})
public final class DefinitionsAdapter extends androidx.recyclerview.widget.RecyclerView.Adapter<com.example.android.definitions.ui.DefinitionViewHolder> {
    @org.jetbrains.annotations.NotNull()
    private java.util.List<com.example.android.definitions.domain.Definition> definitions;
    private final com.example.android.definitions.ui.DefinitionClick callback = null;
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<com.example.android.definitions.domain.Definition> getDefinitions() {
        return null;
    }
    
    public final void setDefinitions(@org.jetbrains.annotations.NotNull()
    java.util.List<com.example.android.definitions.domain.Definition> value) {
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public com.example.android.definitions.ui.DefinitionViewHolder onCreateViewHolder(@org.jetbrains.annotations.NotNull()
    android.view.ViewGroup parent, int viewType) {
        return null;
    }
    
    @java.lang.Override()
    public int getItemCount() {
        return 0;
    }
    
    @java.lang.Override()
    public void onBindViewHolder(@org.jetbrains.annotations.NotNull()
    com.example.android.definitions.ui.DefinitionViewHolder holder, int position) {
    }
    
    public DefinitionsAdapter(@org.jetbrains.annotations.NotNull()
    com.example.android.definitions.ui.DefinitionClick callback) {
        super();
    }
}