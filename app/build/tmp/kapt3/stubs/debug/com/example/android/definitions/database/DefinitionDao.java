package com.example.android.definitions.database;

import java.lang.System;

@androidx.room.Dao()
@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0002\bg\u0018\u00002\u00020\u0001J\u0018\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u00032\b\b\u0002\u0010\u0005\u001a\u00020\u0006H\'J\u0018\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00040\u00032\b\b\u0002\u0010\u0005\u001a\u00020\u0006H\'J\u0018\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00040\u00032\b\b\u0002\u0010\u0005\u001a\u00020\u0006H\'J\u0016\u0010\t\u001a\u00020\n2\f\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003H\'\u00a8\u0006\f"}, d2 = {"Lcom/example/android/definitions/database/DefinitionDao;", "", "getDefinitions", "", "Lcom/example/android/definitions/database/DatabaseDefinition;", "searchedWord", "", "getDefinitionsOrderyByThumbsDown", "getDefinitionsOrderyByThumbsUp", "insertAll", "", "definitions", "app_debug"})
public abstract interface DefinitionDao {
    
    @org.jetbrains.annotations.NotNull()
    @androidx.room.Query(value = "select * from databasedefinition where searched_word like :searchedWord order by thumbs_up desc")
    public abstract java.util.List<com.example.android.definitions.database.DatabaseDefinition> getDefinitions(@org.jetbrains.annotations.NotNull()
    java.lang.String searchedWord);
    
    @org.jetbrains.annotations.NotNull()
    @androidx.room.Query(value = "select * from databasedefinition where searched_word like :searchedWord order by thumbs_up desc")
    public abstract java.util.List<com.example.android.definitions.database.DatabaseDefinition> getDefinitionsOrderyByThumbsUp(@org.jetbrains.annotations.NotNull()
    java.lang.String searchedWord);
    
    @org.jetbrains.annotations.NotNull()
    @androidx.room.Query(value = "select * from databasedefinition where searched_word like :searchedWord order by thumbs_down desc")
    public abstract java.util.List<com.example.android.definitions.database.DatabaseDefinition> getDefinitionsOrderyByThumbsDown(@org.jetbrains.annotations.NotNull()
    java.lang.String searchedWord);
    
    @androidx.room.Insert(onConflict = androidx.room.OnConflictStrategy.REPLACE)
    public abstract void insertAll(@org.jetbrains.annotations.NotNull()
    java.util.List<com.example.android.definitions.database.DatabaseDefinition> definitions);
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 3)
    public final class DefaultImpls {
    }
}