package com.example.android.defininitions

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.filters.LargeTest
import androidx.test.runner.AndroidJUnit4
import com.example.android.definitions.R
import com.example.android.definitions.ui.UrbanDictionaryActivity
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import java.lang.Thread.sleep

@RunWith(AndroidJUnit4::class)
@LargeTest
class SearchTest {

    private lateinit var searchTerm: String

    @get:Rule
    var activityRule: ActivityScenarioRule<UrbanDictionaryActivity> = ActivityScenarioRule(UrbanDictionaryActivity::class.java)

    @Before
    fun initValidString() {
        searchTerm = "Glider"
    }

    @Test
    fun performASearch() {
        onView(withId(R.id.word_editext)).perform(typeText(searchTerm), closeSoftKeyboard())
        onView(withId(R.id.search)).perform(click())
        sleep(1500) // TODO Big NO, we should implement CountingIdlingResource
        onView(withId(R.id.recycler_view)).check(matches(isDisplayed()))
    }
}