package com.example.android.definitions.viewmodels

import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import org.junit.Assert.*
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class DefinitionsViewModelTest {
    private val viewModel: DefinitionsViewModel = DefinitionsViewModel(ApplicationProvider.getApplicationContext())


    @Test
    fun `sanitizeWord should remove special chars`() {
        // GIVEN
        val testWord = "he.-ll/*o"

        // WHEN
        val sanitizedWord = viewModel.sanitizeWord(testWord)

        //THEN
        assertEquals("hello", sanitizedWord)
    }

    @Test
    fun `sanitizeWord should not remove any char`() {
        // GIVEN
        val testWord = "hello123"

        // WHEN
        val sanitizedWord = viewModel.sanitizeWord(testWord)

        //THEN
        assertEquals(testWord, sanitizedWord)
    }

    @Test
    fun `isValidWord should return false when the word is empty`() {
        // GIVEN
        val testWord = ""

        // WHEN
        val isValid = viewModel.isValidWord(testWord)

        //THEN
        assertFalse(isValid)
    }

    @Test
    fun `isValidWord should return false when the word exceeds 32 chars length`() {
        // GIVEN
        val testWord = "asdfghjklqwertyuiopzxcvbnmqwertyr"

        // WHEN
        val isValid = viewModel.isValidWord(testWord)

        //THEN
        assertFalse(isValid)
    }

    @Test
    fun `isValidWord should return true when the word exceeds is valid`() {
        // GIVEN
        val testWord = "rainbow"

        // WHEN
        val isValid = viewModel.isValidWord(testWord)

        //THEN
        assertTrue(isValid)
    }

}