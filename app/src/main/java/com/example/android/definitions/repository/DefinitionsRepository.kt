package com.example.android.definitions.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.android.definitions.database.DefinitionsDatabase
import com.example.android.definitions.database.asDomainModel
import com.example.android.definitions.domain.Definition
import com.example.android.definitions.network.UrbanDictionaryNetwork
import com.example.android.definitions.network.asDatabaseModel
import com.example.android.definitions.network.asDomainModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import timber.log.Timber

class DefinitionsRepository(private val database: DefinitionsDatabase) {
    private var lastSearchedTerm = ""
    private val _definitions = MutableLiveData<List<Definition>>()
    val definitions: LiveData<List<Definition>>
        get() = _definitions

    suspend fun searchDefinitions(searchedWord: String) {
        withContext(Dispatchers.IO) {
            Timber.d("Search for definitions for $searchedWord");

            lastSearchedTerm = searchedWord
            var definitions = database.definitionDao.getDefinitions(searchedWord).asDomainModel()

            if (definitions.isNullOrEmpty()) { // TODO We could add an expiring cache in the future
                Timber.d("Network call is required");

                val networkDefinitions = UrbanDictionaryNetwork.urbanDictionary.getDefinition(searchedWord)
                database.definitionDao.insertAll(networkDefinitions.asDatabaseModel(searchedWord))
                definitions = networkDefinitions.asDomainModel()
            }

            withContext(Dispatchers.Main) {
                _definitions.value = definitions
            }
        }
    }

    suspend fun sortBy(sortBy: SortBy) {
        Timber.d("Will order by $sortBy");

        var sortedDefinitions: List<Definition> = emptyList()

        withContext(Dispatchers.IO) {
            sortedDefinitions = when (sortBy) {
                SortBy.THUMBS_UP -> database.definitionDao.getDefinitionsOrderyByThumbsUp(lastSearchedTerm).asDomainModel()
                SortBy.THUMBS_DOWN -> database.definitionDao.getDefinitionsOrderyByThumbsDown(lastSearchedTerm).asDomainModel()
            }
        }
        withContext(Dispatchers.Main) {
            _definitions.value = sortedDefinitions
        }
    }

    enum class SortBy {
        THUMBS_UP,
        THUMBS_DOWN
    }
}
