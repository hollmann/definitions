package com.example.android.definitions.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.android.definitions.R

class UrbanDictionaryActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_urban_dictionary)
    }
}
