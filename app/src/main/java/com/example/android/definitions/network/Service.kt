package com.example.android.definitions.network

import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query

// Since we only have one service, this can all go in one file.

interface UrbanDictionaryService {
    // TODO Secure the API KEY by obfuscating and moving it out from here.
    @Headers("x-rapidapi-key: 8c992b37c0msh8335a7b0bd0030dp17be07jsn05e41e7d404d")
    @GET("define")
    suspend fun getDefinition(@Query("term") word: String): NetworkDefinitionContainer
}

object UrbanDictionaryNetwork {
    private val retrofit = Retrofit.Builder()
            .baseUrl("https://mashape-community-urban-dictionary.p.rapidapi.com/")
            .addConverterFactory(MoshiConverterFactory.create(Moshi.Builder()
                    .add(KotlinJsonAdapterFactory())
                    .build()))
            .build()

    val urbanDictionary = retrofit.create(UrbanDictionaryService::class.java)

}


