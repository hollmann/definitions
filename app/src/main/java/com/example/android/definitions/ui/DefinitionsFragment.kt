package com.example.android.definitions.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.android.definitions.R
import com.example.android.definitions.databinding.DefinitionItemBinding
import com.example.android.definitions.databinding.FragmentDefinitionsBinding
import com.example.android.definitions.domain.Definition
import com.example.android.definitions.viewmodels.DefinitionsViewModel

class DefinitionsFragment : Fragment() {

    private val viewModel: DefinitionsViewModel by lazy {
        val activity = requireNotNull(this.activity) {
            "You can only access the viewModel after onActivityCreated()"
        }
        ViewModelProvider(this, DefinitionsViewModel.Factory(activity.application))
                .get(DefinitionsViewModel::class.java)
    }

    private var viewModelAdapter: DefinitionsAdapter? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.definitions.observe(viewLifecycleOwner, Observer<List<Definition>> { definitions ->
            definitions?.apply {
                viewModelAdapter?.definitions = definitions
            }
        })
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val binding: FragmentDefinitionsBinding = DataBindingUtil.inflate(
                inflater,
                R.layout.fragment_definitions,
                container,
                false)

        binding.lifecycleOwner = viewLifecycleOwner
        binding.viewModel = viewModel

        viewModelAdapter = DefinitionsAdapter(DefinitionClick {
            Toast.makeText(activity, getString(R.string.more_features_coming_soon), Toast.LENGTH_LONG).show()
        })

        binding.root.findViewById<RecyclerView>(R.id.recycler_view).apply {
            layoutManager = LinearLayoutManager(context)
            adapter = viewModelAdapter
        }

        viewModel.eventNetworkError.observe(viewLifecycleOwner, Observer { isNetworkError ->
            if (isNetworkError) onNetworkError()
        })

        viewModel.eventIsValidWord.observe(viewLifecycleOwner, Observer { isValidWord ->
            if (!isValidWord) onInvalidWord()
        })

        return binding.root
    }

    private fun onNetworkError() {
        viewModel.isNetworkErrorShown.value?.let { isShown ->
            if (!isShown) {
                Toast.makeText(activity, getString(R.string.network_error_message), Toast.LENGTH_LONG).show()
                viewModel.onNetworkErrorShown()
            }
        }
    }

    private fun onInvalidWord() {
        viewModel.isInvalidWordShown.value?.let { isShown ->
            if (!isShown) {
                Toast.makeText(activity, getString(R.string.invalid_word), Toast.LENGTH_LONG).show()
                viewModel.onInvalidWordShown()
            }
        }
    }
}

class DefinitionClick(val showToast: (Definition) -> Unit) {

    fun onClick(definition: Definition) = showToast(definition)
}

/**
 * As this is a very simple app we can keep the the adapter here. If this grows we
 * will create a new file.
 */
class DefinitionsAdapter(private val callback: DefinitionClick) : RecyclerView.Adapter<DefinitionViewHolder>() {

    var definitions: List<Definition> = emptyList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DefinitionViewHolder {
        val withDataBinding: DefinitionItemBinding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                DefinitionViewHolder.LAYOUT,
                parent,
                false)
        return DefinitionViewHolder(withDataBinding)
    }

    override fun getItemCount() = definitions.size

    override fun onBindViewHolder(holder: DefinitionViewHolder, position: Int) {
        holder.viewDataBinding.also {
            it.definition = definitions[position]
            it.definitionCallback = callback
        }
    }

}

/**
 * As this is a very simple app we can keep the the viewholder here. If this grows we
 * will create a new file.
 */
class DefinitionViewHolder(val viewDataBinding: DefinitionItemBinding) :
        RecyclerView.ViewHolder(viewDataBinding.root) {
    companion object {
        @LayoutRes
        val LAYOUT = R.layout.definition_item
    }
}