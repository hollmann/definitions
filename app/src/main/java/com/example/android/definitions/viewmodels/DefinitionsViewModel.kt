package com.example.android.definitions.viewmodels

import android.app.Application
import androidx.annotation.VisibleForTesting
import androidx.lifecycle.*
import com.example.android.definitions.database.getDatabase
import com.example.android.definitions.repository.DefinitionsRepository
import kotlinx.coroutines.launch
import java.io.IOException

class DefinitionsViewModel(application: Application) : AndroidViewModel(application) {

    private val definitionsRepository = DefinitionsRepository(getDatabase(application))

    val definitions = definitionsRepository.definitions

    private var _eventNetworkError = MutableLiveData(false)
    val eventNetworkError: LiveData<Boolean>
        get() = _eventNetworkError


    private var _isNetworkErrorShown = MutableLiveData(false)
    val isNetworkErrorShown: LiveData<Boolean>
        get() = _isNetworkErrorShown

    private var _isValidWord = MutableLiveData(true)
    val eventIsValidWord: LiveData<Boolean>
        get() = _isValidWord

    private var _isInvalidWordShown = MutableLiveData(false)
    val isInvalidWordShown: LiveData<Boolean>
        get() = _isInvalidWordShown

    private var _isLoading = MutableLiveData(false)
    val isLoading: LiveData<Boolean>
        get() = _isLoading

    var checkedRadioButton = MutableLiveData(DefinitionsRepository.SortBy.THUMBS_UP)

    val searchWord = MutableLiveData<String>()

    init {
        refreshDataFromRepository()
    }

    private fun refreshDataFromRepository(word: String = "rainbow") {
        viewModelScope.launch {
            try {
                _isLoading.value = true
                definitionsRepository.searchDefinitions(word)
                _eventNetworkError.value = false
                _isNetworkErrorShown.value = false

            } catch (networkError: IOException) {
                if (definitions.value.isNullOrEmpty()) {
                    _eventNetworkError.value = true
                }
            }
            _isLoading.value = false
        }
    }

    fun onNetworkErrorShown() {
        _isNetworkErrorShown.value = true
    }

    fun onInvalidWordShown() {
        _isInvalidWordShown.value = true
    }

    fun onSearchClicked() {
        if (isValidWord(searchWord.value)) {
            searchWord.value?.let { word ->
                checkedRadioButton.value = DefinitionsRepository.SortBy.THUMBS_UP
                refreshDataFromRepository(sanitizeWord(word))
            }
        } else {
            _isValidWord.value = false
        }
    }

    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE) // TODO We should access to private methods through public interfaces
    fun sanitizeWord(word: String) = word.replace(Regex(NO_SPECIAL_CHARS_REGEX), "").trim()

    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE) // TODO We should access to private methods through public interfaces
    fun isValidWord(word: String?) = !word.isNullOrBlank() && word.length <= WORD_MAX_LENGTH


    fun sortByThumbsUp() {
        checkedRadioButton.value = DefinitionsRepository.SortBy.THUMBS_UP
        viewModelScope.launch {
            definitionsRepository.sortBy(DefinitionsRepository.SortBy.THUMBS_UP)
        }
    }

    fun sortByThumbsDown() {
        checkedRadioButton.value = DefinitionsRepository.SortBy.THUMBS_DOWN

        viewModelScope.launch {
            definitionsRepository.sortBy(DefinitionsRepository.SortBy.THUMBS_DOWN)
        }
    }

    class Factory(private val app: Application) : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(DefinitionsViewModel::class.java)) {
                @Suppress("UNCHECKED_CAST")
                return DefinitionsViewModel(app) as T
            }
            throw IllegalArgumentException("Unable to construct viewmodel")
        }
    }

    companion object {
        const val WORD_MAX_LENGTH = 32
        const val NO_SPECIAL_CHARS_REGEX = "[^0-9a-zA-Z]+"
    }
}
