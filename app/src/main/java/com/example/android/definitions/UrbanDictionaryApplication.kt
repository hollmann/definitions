package com.example.android.definitions

import android.app.Application
import timber.log.Timber

class UrbanDictionaryApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        Timber.plant(Timber.DebugTree())
    }
}
