package com.example.android.definitions.database

import android.content.Context
import androidx.room.*

@Dao
interface DefinitionDao {
    @Query("select * from databasedefinition where searched_word like :searchedWord order by thumbs_up desc")
    fun getDefinitions(searchedWord: String = ""): List<DatabaseDefinition>

    @Query("select * from databasedefinition where searched_word like :searchedWord order by thumbs_up desc")
    fun getDefinitionsOrderyByThumbsUp(searchedWord: String = ""): List<DatabaseDefinition>

    @Query("select * from databasedefinition where searched_word like :searchedWord order by thumbs_down desc")
    fun getDefinitionsOrderyByThumbsDown(searchedWord: String = ""): List<DatabaseDefinition>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(definitions: List<DatabaseDefinition>)
}


@Database(entities = [DatabaseDefinition::class], version = 1)
abstract class DefinitionsDatabase : RoomDatabase() {
    abstract val definitionDao: DefinitionDao
}

private lateinit var INSTANCE: DefinitionsDatabase

fun getDatabase(context: Context): DefinitionsDatabase {
    synchronized(DefinitionsDatabase::class.java) {
        if (!::INSTANCE.isInitialized) {
            INSTANCE = Room.databaseBuilder(context.applicationContext,
                    DefinitionsDatabase::class.java,
                    "definitions_database").build()
        }
    }
    return INSTANCE
}
