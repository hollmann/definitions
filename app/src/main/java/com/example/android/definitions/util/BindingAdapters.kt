package com.example.android.definitions.util

import android.view.View
import androidx.databinding.BindingAdapter


@BindingAdapter("isNetworkError")
fun hideIfNetworkError(view: View, isNetWorkError: Boolean) {
    if (isNetWorkError) {
        view.visibility = View.GONE
    }
}

@BindingAdapter("displayWhenLoading")
fun displayWhenLoading(view: View, isLoading: Boolean?) {
    view.visibility = if (isLoading == true) View.VISIBLE else View.GONE
}

@BindingAdapter("hideWhenLoading")
fun hideWhenLoading(view: View, isLoading: Boolean?) {
    view.visibility = if (isLoading == true) View.GONE else View.VISIBLE
}