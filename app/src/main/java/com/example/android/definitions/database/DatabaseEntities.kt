package com.example.android.definitions.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.android.definitions.domain.Definition

@Entity
data class DatabaseDefinition constructor(
        @PrimaryKey
        val defid: Integer,
        val definition: String,
        val word: String,
        @ColumnInfo(name = "thumbs_up")
        val thumbsUp: Integer,
        @ColumnInfo(name = "thumbs_down")
        val thumbsDown: Integer,
        val writtenOn: String,
        val author: String,
        val example: String,
        @ColumnInfo(name = "searched_word")
        val searchedWord: String)


fun List<DatabaseDefinition>.asDomainModel(): List<Definition> {
    return map { it ->
        Definition(
                definition = it.definition,
                word = it.word,
                thumbsUp = it.thumbsUp,
                thumbsDown = it.thumbsDown,
                writtenOn = it.writtenOn,
                example = it.example,
                author = it.author)
    }
}
