package com.example.android.definitions.network

import com.example.android.definitions.database.DatabaseDefinition
import com.example.android.definitions.domain.Definition
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class NetworkDefinitionContainer(val list: List<NetworkDefinition>)

@JsonClass(generateAdapter = true)
data class NetworkDefinition(
        val defid: Integer,
        val definition: String,
        val word: String,
        @Json(name = "thumbs_up")
        val thumbsUp: Integer,
        @Json(name = "thumbs_down")
        val thumbsDown: Integer,
        @Json(name = "written_on")
        val writtenOn: String,
        val author: String,
        val example: String)

fun NetworkDefinitionContainer.asDomainModel(): List<Definition> {
    return list.map {
        Definition(
                definition = it.definition,
                word = it.word,
                thumbsUp = it.thumbsUp,
                thumbsDown = it.thumbsDown,
                writtenOn = it.writtenOn,
                example = it.example,
                author = it.author)
    }
}

fun NetworkDefinitionContainer.asDatabaseModel(searchedWord: String): List<DatabaseDefinition> {
    return list.map {
        DatabaseDefinition(
                defid = it.defid,
                definition = it.definition,
                word = it.word,
                thumbsUp = it.thumbsUp,
                thumbsDown = it.thumbsDown,
                writtenOn = it.writtenOn,
                example = it.example,
                author = it.author,
                searchedWord = searchedWord)
    }
}

