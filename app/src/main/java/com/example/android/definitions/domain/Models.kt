package com.example.android.definitions.domain

data class Definition(val definition: String,
                      val word: String,
                      val thumbsUp: Integer,
                      val thumbsDown: Integer,
                      val writtenOn: String,
                      val author: String,
                      val example: String)